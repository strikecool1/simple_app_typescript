"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
class App {
    constructor(appInit) {
        this.app = express();
        this.port = appInit.port;
        this.middlewares(appInit.middleWares);
        this.routes(appInit.controllers);
        this.assets();
        this.template();
    }
    // получаем список middlewa
    middlewares(middleWares) {
        middleWares.forEach(middleWare => {
            this.app.use(middleWare);
        });
    }
    // получаем список роутов
    routes(controllers) {
        controllers.forEach(controller => {
            this.app.use('/', controller.router);
        });
    }
    // подключаем статические файлы
    assets() {
        this.app.use(express.static('public'));
        this.app.use(express.static('views'));
    }
    // подключаем шаблон
    template() {
        this.app.set('view engine', 'pug');
    }
    // прослушиваем порт
    listen() {
        this.app.listen(this.port, () => {
            console.log(`Приложение запущено http://localhost:${this.port}`);
        });
    }
}
exports.default = App;
//# sourceMappingURL=app.js.map