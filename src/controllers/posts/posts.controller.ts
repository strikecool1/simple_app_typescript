import * as express from 'express'
import { Request, Response } from 'express'
import IControllerBase from 'interfaces/IControllerBase.interface'

interface IPost {
    id: number
    author: string
    content: string
    title: string
}

class PostsController implements IControllerBase {
    private path = '/posts'
    private router = express.Router()

    private posts: IPost[] = [
        {
            id: 1,
            author: 'Ali GOREN',
            content: 'This is an example post',
            title: 'Hello world!'
        }
    ]

    constructor() {
        this.initRoutes()
    }

    public initRoutes(): void {
        this.router.get(this.path + '/:id', this.getPost)
        this.router.get(this.path, this.getAllPosts)
        this.router.post(this.path, this.createPost)
    }

    private getPost = (req: Request, res: Response): void => {
        const id =+ req.params.id
        let result = this.posts.find(post => post.id == id)

        if (!result) {
            res.status(404).send({
                'error': 'Post not found!'
            })
        }
        
        res.render('pages/post', result)
    }

    private getAllPosts = (req: Request, res: Response): void => {
        res.send(this.posts)
    }

    private createPost = (req: Request, res: Response): void => {
        const post: IPost = req.body
        this.posts.push(post)
        res.send(this.posts)
    }
}

export default PostsController