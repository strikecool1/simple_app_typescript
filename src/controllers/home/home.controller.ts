import * as express from 'express';
import { Request, Response } from 'express';
import IControllerBase from 'interfaces/IControllerBase.interface';


class HomeController implements IControllerBase {
    private path = '/';
    private router = express.Router();
    
    constructor() {
        this.initRoutes();
    }

    public initRoutes(): void { this.router.get('/', this.index); } 


    private index = (req: Request, res: Response): void => {
        
        const users = [
            {
                id: 1,
                name: 'Ali'
            },
            {
                id: 2,
                name: 'Can'
            },
            {
                id: 3,
                name: 'Ahmet'
            }
        ]

        res.render('pages/home', { users });
    }
}

export default HomeController;