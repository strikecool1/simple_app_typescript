import * as express from 'express'
import { Application } from 'express'

class App {
    public app: Application
    public port: number

    constructor(appInit: { port: number; middleWares: any; controllers: any; }) {
        this.app = express()
        this.port = appInit.port

        this.middlewares(appInit.middleWares)
        this.routes(appInit.controllers)
        this.assets()
        this.template()
    }
    // получаем список middlewa
    private middlewares(middleWares: { forEach: (arg0: (middleWare: any) => void) => void; }) {
        middleWares.forEach(middleWare => {
            this.app.use(middleWare)
        })
    }
    // получаем список роутов
    private routes(controllers: { forEach: (arg0: (controller: any) => void) => void; }) {
        controllers.forEach(controller => {
            this.app.use('/', controller.router)
        })
    }

    // подключаем статические файлы
    private assets() {
        this.app.use(express.static('public'))
        this.app.use(express.static('views'))
    }
    // подключаем шаблон
    private template() {
        this.app.set('view engine', 'pug')
    }
    // прослушиваем порт
    public listen() {
        this.app.listen(this.port, () => {
            console.log(`Приложение запущено http://localhost:${this.port}`)
        })
    }
}

export default App